//Model for the game
class Game {
  late int id;
  late String title;
  late String thumbnail;
  late String shortDescription;
  late String gameUrl;
  late String genre;
  late String platform;
  late String publisher;
  late String developer;
  late String releaseDate;
  late String freetogameProfileUrl;

  Game(
      {required this.id,
      required this.title,
      required this.thumbnail,
      required this.shortDescription,
      required this.gameUrl,
      required this.genre,
      required this.platform,
      required this.publisher,
      required this.developer,
      required this.releaseDate,
      required this.freetogameProfileUrl});

  factory Game.fromJson(Map<String, dynamic> json) {
    return Game(
      id: json['id'],
      title: json['title'],
      thumbnail: json['thumbnail'],
      shortDescription: json['short_description'],
      gameUrl: json['game_url'],
      genre: json['genre'],
      platform: json['platform'],
      publisher: json['publisher'],
      developer: json['developer'],
      releaseDate: json['release_date'],
      freetogameProfileUrl: json['freetogame_profile_url'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['thumbnail'] = this.thumbnail;
    data['short_description'] = this.shortDescription;
    data['game_url'] = this.gameUrl;
    data['genre'] = this.genre;
    data['platform'] = this.platform;
    data['publisher'] = this.publisher;
    data['developer'] = this.developer;
    data['release_date'] = this.releaseDate;
    data['freetogame_profile_url'] = this.freetogameProfileUrl;
    return data;
  }
}

List<Game> games = [
  Game(
      id: 1,
      title: "Dauntless",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless"),
  Game(
      id: 2,
      title: "Pac Man",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless"),
  Game(
      id: 3,
      title: "Grand Teft Auto",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless"),
  Game(
      id: 4,
      title: "Pong",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless"),
  Game(
      id: 5,
      title: "Age Of Empires I",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless"),
  Game(
      id: 6,
      title: "Age Of Empires II",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless"),
  Game(
      id: 7,
      title: "Civilization I",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless"),
  Game(
      id: 8,
      title: "Civilization II",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless"),
  Game(
      id: 9,
      title: "Civilization III",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless"),
  Game(
      id: 10,
      title: "Moto GP",
      thumbnail: "https:\/\/www.freetogame.com\/g\/1\/thumbnail.jpg",
      shortDescription:
          "A free-to-play, co-op action RPG with gameplay similar to Monster Hunter.",
      gameUrl: "https:\/\/www.freetogame.com\/open\/dauntless",
      genre: "MMORPG",
      platform: "PC (Windows)",
      publisher: "Phoenix Labs",
      developer: "Phoenix Labs, Iron Galaxy",
      releaseDate: "2019-05-21",
      freetogameProfileUrl: "https:\/\/www.freetogame.com\/dauntless")
];
