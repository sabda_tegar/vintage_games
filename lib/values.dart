import 'package:flutter/material.dart';

class ThemeColor {
  static const colorMain = Color(0xFF535353);
  static const colorMainLight = Color(0xFFACACAC);
}

class ThemePadding {
  static const defaultPadding = 20.0;
}
