import 'package:flutter/material.dart';
import 'package:vintage_games/values.dart';

//class to show every item of gridview
class ViewGames extends StatefulWidget {
  final game;
  final selected;
  final index;

  ViewGames({Key? key, this.game, this.selected, this.index}) : super(key: key);

  @override
  State<ViewGames> createState() => ViewGamesState();
}

class ViewGamesState extends State<ViewGames> {
  int _selectedIndex = -1;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            Container(
              child: Stack(
                children: <Widget>[
                  buildHero(widget: widget),
                  buildFavoriteIcon()
                ],
              ),
            ),
            buildItemText(widget: widget)
          ],
        ),
      ],
    );
  }

  //Widget to Show FavoriteIcon
  Padding buildFavoriteIcon() {
    return Padding(
        padding: EdgeInsets.all(5.0),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          (_selectedIndex != null && _selectedIndex == widget.index)
              ? IconButton(
                  onPressed: () {
                    _onSelected(widget.index, false);
                  },
                  icon: Icon(Icons.favorite, color: Colors.red),
                  iconSize: 35,
                )
              : IconButton(
                  onPressed: () {
                    _onSelected(widget.index, true);
                  },
                  icon: Icon(Icons.favorite_border, color: Colors.red[300]),
                  iconSize: 30)
        ]));
  }

  //fun when Favorite Icon Was Clicked
  _onSelected(int index, bool selected) {
    setState(() {
      selected ? _selectedIndex = index : _selectedIndex = -1;
    });
  }
}

//class Widget to Show Text Bellow The Hero
class buildItemText extends StatelessWidget {
  const buildItemText({
    Key? key,
    required this.widget,
  }) : super(key: key);

  final ViewGames widget;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(ThemePadding.defaultPadding / 2),
        child: Text(widget.game.title,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Glory',
              fontStyle: FontStyle.normal,
            )),
      ),
    );
  }
}

//class Widget to build Hero with BoxDecoration
class buildHero extends StatelessWidget {
  const buildHero({
    Key? key,
    required this.widget,
  }) : super(key: key);

  final ViewGames widget;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Container(
              width: 350,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.red.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 5)
                  ],
                  color: Colors.black),
              foregroundDecoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.black54),
              alignment: Alignment.center,
              margin:
                  EdgeInsets.symmetric(horizontal: ThemePadding.defaultPadding),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Hero(
                  tag: widget.game.title,
                  child: Image.network(
                    widget.game.thumbnail,
                  ),
                ),
              )),
        ),
      ],
    );
  }
}
