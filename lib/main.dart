// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:developer';

import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:vintage_games/game.dart';
import 'package:vintage_games/values.dart';
import 'package:vintage_games/view-games.dart';
import 'package:vintage_games/search_widget.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light,
        statusBarColor: Colors.black),
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Skill Builder',
      theme: ThemeData(fontFamily: 'Glory'),
      home: HomeWidget(),
    );
  }
}

// This is the stateful widget that the main application instantiates.
class HomeWidget extends StatefulWidget {
  @override
  State<HomeWidget> createState() => HomeWidgetState();
}

// /// This is the private State class that goes with HomeWidget
class HomeWidgetState extends State<HomeWidget> {
  //The data holder to display the game according to the query
  List<Game> showGame = games;
  //The data holder from api
  List<Game> masterGame = games;
  //String to query the game
  String query = '';
  late Future<Game> futureGame;

  @override
  void initState() {
    super.initState();
    futureGame = fetchGame();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            actions: <Widget>[],
            pinned: true,
            expandedHeight: 340.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                "Bruce's Retro Games",
                style: TextStyle(color: Colors.white, fontFamily: 'Glory'),
              ),
              centerTitle: true,
              background: Image.network(
                "https://images.unsplash.com/photo-1498736297812-3a08021f206f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=757&q=80",
                fit: BoxFit.fitWidth,
              ),
            ),
            backgroundColor: Colors.black87,
            collapsedHeight: 50,
            toolbarHeight: 0,
          ),
          SliverToBoxAdapter(
            child: buildSearch(),
          ),
          buildGridView(futureGame: futureGame, showGame: showGame),
        ],
      ),
      backgroundColor: Colors.black,
    );
  }

  //Async fun to fetch data from 3rd party API(RESTAPI)
  Future<Game> fetchGame() async {
    final response = await http.get(
        Uri.parse(
            'https://free-to-play-games-database.p.rapidapi.com/api/games'),
        headers: {
          "x-rapidapi-host": "free-to-play-games-database.p.rapidapi.com",
          "x-rapidapi-key": "d9a00635d8msh9fac08cdb3b3320p149968jsn466ae6cc89d6"
        });

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      final data = jsonDecode(response.body) as List;
      final deserializedObjects =
          data.map((dynamic o) => Game.fromJson(o as Map<String, dynamic>));

      masterGame = deserializedObjects.toList();

      setState(() {
        showGame = masterGame;
      });

      return Game.fromJson(jsonDecode(response.body)[0]);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  //Widget to show SearchBar
  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Type game title..',
        onChanged: searchGame,
      );

  //Function to filter games(showGame) by query
  void searchGame(String query) {
    final searchGames = showGame.where((game) {
      final titleLower = game.title.toLowerCase();
      final searchLower = query.toLowerCase();

      return titleLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.showGame = searchGames;
      //if the query is empty, set the showGame like begining
      if (this.query == '') {
        this.showGame = masterGame;
      }
    });
  }
}

//Widget to Populate data to GridView
class buildGridView extends StatelessWidget {
  const buildGridView({
    Key? key,
    required this.futureGame,
    required this.showGame,
  }) : super(key: key);

  final Future<Game> futureGame;
  final List<Game> showGame;

  @override
  Widget build(BuildContext context) {
    return SliverGrid(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 20.0,
        crossAxisSpacing: 20.0,
        childAspectRatio: 1.5,
      ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return Container(
            alignment: Alignment.center,
            child: FutureBuilder<Game>(
              future: futureGame,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ViewGames(
                      game: showGame[index], selected: false, index: index);
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }

                // By default, show a loading spinner.
                return const CircularProgressIndicator();
              },
            ),
          );
        },
        childCount: showGame.length,
      ),
    );
  }
}
